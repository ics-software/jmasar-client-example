package se.esss.ics.jmasar.client.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import se.esss.ics.masar.model.Config;
import se.esss.ics.masar.model.ConfigPv;
import se.esss.ics.masar.model.Folder;
import se.esss.ics.masar.model.Node;
import se.esss.ics.masar.model.Provider;
import se.esss.ics.masar.model.Snapshot;
import se.esss.ics.masar.model.SnapshotItem;

/**
 * Class demonstrating how to use the jmasar REST service. It uses the Spring
 * {@link RestTemplate} do communicate with the service.
 * 
 * @author georgweiss Created 15 Nov 2018
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

	/**
	 * Change if needed...
	 */
	private String jmasarServiceURL = "http://jmasar-test.esss.lu.se";

	public static void main(String args[]) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) {

		RestTemplate restTemplate = new RestTemplate();

		try {
			testWithPredefinedPVList(args[1], restTemplate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Create a folder in the root folder, with random name to make sure this can
		// be run multiple times.
		Folder folder = createFolder(UUID.randomUUID().toString(), restTemplate);

		// Create a configuration in the new folder
		Config configuration = createConfiguration(folder, restTemplate);

		// Take preliminary snapshot.

		Snapshot snapshot = takePreliminarySnapshot(configuration, restTemplate);

		snapshot = commitSnapshot(snapshot.getId(), "Snapshot name", "My username", "My comment", restTemplate);

		for (SnapshotItem snapshotItem : snapshot.getSnapshotItems()) {
			System.out.println(snapshotItem.getValue());
		}
	}

	/**
	 * 
	 * @param absolutePathToFile
	 * @param restTemplate
	 * @throws Exception
	 */
	private void testWithPredefinedPVList(String absolutePathToFile, RestTemplate restTemplate) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(absolutePathToFile));
		List<String> pvNames = new ArrayList<>();
		while (true) {
			String line = bufferedReader.readLine();
			if (line == null)
				break;
			pvNames.add(line.trim());
		}

		bufferedReader.close();

		Folder folder = createFolder(UUID.randomUUID().toString(), restTemplate);

		List<ConfigPv> configPvs = new ArrayList<>();
		for (String pvName : pvNames) {
			ConfigPv configPv = ConfigPv.builder().provider(Provider.ca).pvName(pvName).build();
			configPvs.add(configPv);
		}

		Config config = Config.builder().configPvList(configPvs).description("Open XAL test")
				.name(UUID.randomUUID().toString()).parentId(folder.getId()).build();

		HttpEntity<Config> request = new HttpEntity<>(config);

		ResponseEntity<Config> response = restTemplate.exchange(jmasarServiceURL + "/config", HttpMethod.PUT, request,
				Config.class);

		config = response.getBody();


		long start = System.currentTimeMillis();
		Snapshot snapshot = takePreliminarySnapshot(config, restTemplate);
		
		long end = System.currentTimeMillis();
		
		System.out.println("Took preliminary snapshot in " + (end - start) + " ms");
		
		snapshot = commitSnapshot(snapshot.getId(), "My snapshot name", "My name", "My comment", restTemplate);
		
		start = System.currentTimeMillis();
		
		snapshot = getSnapshot(snapshot.getId(), restTemplate);
		
		end = System.currentTimeMillis();
		
		System.out.println("Retrieved " + snapshot.getSnapshotItems().size() + " snapshot data items from snapshot in " + (end - start) + " ms");
	}

	/**
	 * This can be called successfully only once per combination of parent node and
	 * folder name.
	 * 
	 * @param restTemplate
	 * @return The created folder, i.e. including database id and create date.
	 */
	private Folder createFolder(String folderName, RestTemplate restTemplate) {

		Folder folder = Folder.builder().name(folderName).parentId(Node.ROOT_NODE_ID).build();
		// RestTemplate has postForObject(), but not putForObject().
		// Use exchange() instead.
		HttpEntity<Folder> request = new HttpEntity<>(folder);
		ResponseEntity<Folder> response = restTemplate.exchange(jmasarServiceURL + "/folder", HttpMethod.PUT, request,
				Folder.class);

		return response.getBody();
	}

	private Config createConfiguration(Folder parent, RestTemplate restTemplate) {

		ConfigPv configPv1 = ConfigPv.builder().provider(Provider.ca).pvName("TEMP").build();

		ConfigPv configPv2 = ConfigPv.builder().provider(Provider.ca).pvName("COUNTER").build();

		Config config = Config.builder().configPvList(Arrays.asList(configPv1, configPv2)).description("description")
				.name(UUID.randomUUID().toString()).parentId(parent.getId()).build();

		HttpEntity<Config> request = new HttpEntity<>(config);

		ResponseEntity<Config> response = restTemplate.exchange(jmasarServiceURL + "/config", HttpMethod.PUT, request,
				Config.class);

		return response.getBody();
	}

	/**
	 * Takes a preliminary snapshot. Note: if the PV(s) specified in the
	 * configuration are not available, a preliminary snapshot is created and
	 * persisted anyway. The fetchStatus field of each SnapshotPv of the snapshot
	 * should be inspected to determine if the read operation has been successful.
	 * Snapshots that are preliminary are not visible when listing snapshots for a
	 * configuration.
	 * 
	 * @param config
	 * @param restTemplate
	 * @return
	 */
	private Snapshot takePreliminarySnapshot(Config config, RestTemplate restTemplate) {

		HttpEntity<Config> request = new HttpEntity<>(config);

		ResponseEntity<Snapshot> response = restTemplate.exchange(jmasarServiceURL + "/snapshot/" + config.getId(),
				HttpMethod.PUT, request, Snapshot.class);

		return response.getBody();

	}

	/**
	 * Commit the snapshot, i.e. make it visible when listing snapshots for a
	 * configuration.
	 * 
	 * @param snapshotId
	 * @param userName
	 *            Mandatory
	 * @param comment
	 *            Mandatory
	 * @param restTemplate
	 * @return
	 */
	private Snapshot commitSnapshot(int snapshotId, String snapshotName, String userName, String comment,
			RestTemplate restTemplate) {

		ResponseEntity<Snapshot> response = restTemplate.exchange(jmasarServiceURL + "/snapshot/" + snapshotId
				+ "/?userName=" + userName + "&comment=" + comment + "&snapshotName=" + snapshotName, HttpMethod.POST,
				null, Snapshot.class);

		return response.getBody();
	}
	
	
	private Snapshot getSnapshot(int snapshotId, RestTemplate restTemplate) {
		ResponseEntity<Snapshot> response = restTemplate.exchange(jmasarServiceURL + "/snapshot/" + snapshotId, HttpMethod.GET,
				null, Snapshot.class);

		return response.getBody();
	}

}
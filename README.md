Demonstrates how to use the JMasar REST service from a Java client.

The application is based on Spring Boot and uses the Spring Framework RestTemplate class to do the magic. 
However, the key aspect to understand is how to use the REST end points and the classes in jmasar-model (being the Java API), see 
https://gitlab.esss.lu.se/ics-software/jmasar-model

Another resource to get acquainted with is the Swagger documentation, found here:
http://jmasar-test.esss.lu.se/swagger-ui.html

